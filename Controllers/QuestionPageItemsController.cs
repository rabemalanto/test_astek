using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using board.domain.Concrete;
using board.domain.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace board.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  public class QuestionPageItemsController : ControllerBase
    {
        private readonly QuestionContext _context;

        public QuestionPageItemsController(QuestionContext context)
        {
            _context = context;
        }

        // GET: api/QuestionPageItems
        [HttpGet]
        public IEnumerable<QuestionPageItem> GetquestionPageItem()
        {
            return _context.questionPageItem;
        }

        [HttpGet("{idQuestionPage}")]
        public async Task<IActionResult> GetQuestionPageItem([FromRoute] Guid idQuestionPage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var questionPageItems = _context.questionPageItem.Where(_=>_.idQuestionPage == idQuestionPage);

            if (questionPageItems == null)
            {
                return NotFound();
            }

            return Ok(questionPageItems);
        }

        // PUT: api/QuestionPageItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuestionPageItem([FromRoute] Guid id, [FromBody] QuestionPageItem questionPageItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != questionPageItem.id)
            {
                return BadRequest();
            }

            _context.Entry(questionPageItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionPageItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/QuestionPageItems
        [HttpPost]
        public async Task<IActionResult> PostQuestionPageItem([FromBody] QuestionPageItem questionPageItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.questionPageItem.Add(questionPageItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetQuestionPageItem", new { id = questionPageItem.id }, questionPageItem);
        }

        // DELETE: api/QuestionPageItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteQuestionPageItem([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var questionPageItem = await _context.questionPageItem.FindAsync(id);
            if (questionPageItem == null)
            {
                return NotFound();
            }

            _context.questionPageItem.Remove(questionPageItem);
            await _context.SaveChangesAsync();

            return Ok(questionPageItem);
        }

        private bool QuestionPageItemExists(Guid id)
        {
            return _context.questionPageItem.Any(e => e.id == id);
        }
    }
}
