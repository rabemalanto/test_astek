using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using board.domain.Concrete;
using board.domain.Entities;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace board.Controllers
{
    [Route("api/questionnaires")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  public class QuestionsController : ControllerBase
    {
        private readonly QuestionContext _context;
    public QuestionsController(QuestionContext context)
        {
            _context = context;
        }

    // GET: api/questionnaire
    [HttpGet]
    public IEnumerable<Question> Getquestion()
    {
      var test =  (from q in _context.question
              //join qp in _context.questionPage on q.id equals qp.idQuestion
              where q.idUserCreated == Guid.Parse(User.FindFirstValue("USERID"))
                   select q);

      return test;
    }

    // GET: api/questionnaire/5
    [HttpGet("{id}")]
        public async Task<IActionResult> GetQuestion([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Question question = await _context.question.FindAsync(id);
            //IQueryable<QuestionPage> questionPages = _context.questionPage.Where(x => x.idQuestion == question.id);

           /*await questionPages.ForEachAsync(x=> 
              x.QuestionPageItems = _context.questionPageItem.Where(_ => _.idQuestionPage == x.id)
            );*/
          //question.QuestionPages = questionPages;
            if (question == null)
            {
                return NotFound();
            }

            return Ok(question);
        }

        // PUT: api/Questions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuestion([FromRoute] Guid id, [FromBody] Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != question.id)
            {
                return BadRequest();
            }

            _context.Entry(question).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(question);
        }

        // POST: api/Questions
        [HttpPost]
        public async Task<IActionResult> PostQuestion([FromBody] Question question)
        {
      question.id = Guid.NewGuid();
      question.idUserCreated = Guid.Parse(User.FindFirstValue("USERID"));
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _context.question.Add(question);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetQuestion", new { id = question.id }, question);
        }

        // DELETE: api/Questions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteQuestion([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var question = await _context.question.FindAsync(id);
            if (question == null)
            {
                return NotFound();
            }

            _context.question.Remove(question);
            await _context.SaveChangesAsync();

            return Ok(_context.question);
        }

        private bool QuestionExists(Guid id)
        {
            return _context.question.Any(e => e.id == id);
        }
    }
}
