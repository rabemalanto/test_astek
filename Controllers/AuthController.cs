using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using board.domain.Concrete;
using board.domain.Entities;
using board.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;

namespace board.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class AuthController : Controller
    {
    private readonly QuestionContext _questionContext;
    private readonly IConfiguration _configuration; 
    public AuthController(QuestionContext questionContext, IConfiguration c)
    {
      _questionContext = questionContext;
      _configuration = c;
    }
    
    [HttpPost("/api/auth/login")]
    public async Task<IActionResult> Login([FromBody] LoginViewModel creds)
    {
      
      if (ModelState.IsValid)
      {
        dynamic reponseLogin = DoLogin(creds);
        if (!string.IsNullOrEmpty(reponseLogin.Token))
        {
            HttpContext.Session.SetString("JWToken", (string)reponseLogin.Token);
          
          return Ok(reponseLogin);
        }
        
      }
      return null;
    }
    [HttpPost("/api/auth/logout")]
    public async Task<IActionResult> Logout()
    {
      HttpContext.Session.Clear();
      return Ok();
    }
    private object DoLogin(LoginViewModel creds)
    {
      User user = _questionContext.user.FirstOrDefault(x=>(x.nom == creds.Name || x.email == creds.Name) && x.pasword == creds.Password);
      if (user != null)
      {
        var key = Encoding.ASCII.GetBytes(_configuration["Secret"]);
          var JWToken = new JwtSecurityToken(
                    issuer: _configuration["Host"],
                    audience: _configuration["Host"],
                    claims: GetUserClaims(user),
                    notBefore: new DateTimeOffset(DateTime.Now).DateTime,
                    expires: new DateTimeOffset(DateTime.Now.AddDays(1)).DateTime,
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
          );

          var token = new JwtSecurityTokenHandler().WriteToken(JWToken);
          return new { Token = token, User = user  };
        
      }

      return null;
    }

    private IEnumerable<Claim> GetUserClaims(User user)
    {
      List<Claim> claims = new List<Claim>();
      Claim _claim;
      _claim = new Claim(ClaimTypes.Name, $"{user.nom} {user.prenom}");
      claims.Add(_claim);
      _claim = new Claim("USERID", user.id.ToString());
      claims.Add(_claim);
      _claim = new Claim("EMAILID", user.email);
      claims.Add(_claim);
      
      return claims.AsEnumerable<Claim>();
    }
  }

}
