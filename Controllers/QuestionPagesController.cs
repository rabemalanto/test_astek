using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using board.domain.Concrete;
using board.domain.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace board.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  public class QuestionPagesController : ControllerBase
    {
        private readonly QuestionContext _context;

        public QuestionPagesController(QuestionContext context)
        {
            _context = context;
        }
        
        // GET: api/QuestionPages
        [HttpGet]
        public IEnumerable<QuestionPage> GetquestionPage()
        {
            return _context.questionPage;
        }

        // GET: api/QuestionPages/5
        [HttpGet("{id}")]
        public IActionResult GetQuestionPage([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IEnumerable<QuestionPage> questionPage =_context.questionPage.Where(_ => _.idQuestion == id);
      
            if (questionPage == null)
            {
                return NotFound();
            }

            return Ok(questionPage.ToList());
        }
        
        // PUT: api/QuestionPages/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuestionPage([FromRoute] Guid id, [FromBody] QuestionPage questionPage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != questionPage.id)
            {
                return BadRequest();
            }

            _context.Entry(questionPage).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionPageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/QuestionPages
        [HttpPost]
        public async Task<IActionResult> PostQuestionPage([FromBody] QuestionPage questionPage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            _context.questionPage.Add(questionPage);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetQuestionPage", new { id = questionPage.id }, questionPage);
        }

        // DELETE: api/QuestionPages/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteQuestionPage([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var questionPage = await _context.questionPage.FindAsync(id);
            if (questionPage == null)
            {
                return NotFound();
            }
      _context.Entry(questionPage).State = EntityState.Deleted;
      //_context.questionPage.Remove(questionPage);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool QuestionPageExists(Guid id)
        {
            return _context.questionPage.Any(e => e.id == id);
        }
    }
}
