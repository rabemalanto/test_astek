using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace board.domain.Entities
{
  public class Answer
  {
    public Guid id { get; set; }
    [ForeignKey("QuestionPageItem")]
    public Guid idQuestionPageItem { get; set; }
    public string response { get; set; }
    public Guid idUser { get; set; }
    public virtual QuestionPageItem QuestionPageItem { get; set; }
  }
}
