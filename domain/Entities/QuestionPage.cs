using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace board.domain.Entities
{
  public class QuestionPage
  {
    public Guid id { get; set; }
    public string titre { get; set; }
    public int rang { get; set; }
    [ForeignKey("Question")]
    public Guid idQuestion{get;set;}
    public virtual Question Question { get; set; }
    [JsonIgnore]
    public virtual IEnumerable<QuestionPageItem> QuestionPageItems { get; set; }
  }
}
