using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace board.domain.Entities
{
  public class Question
  {
    public Guid id { get; set; }
    public string titre { get; set; }
    public Guid idUserCreated { get; set; }
    public string uri { get; set; }
    [JsonIgnore]
    public virtual IEnumerable<QuestionPage> QuestionPages { get; set; }
  }
}
