using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace board.domain.Entities
{
  public class User
  {
    public Guid id { get; set; }
    public string pasword { get; set; }
    public string prenom { get; set; }
    public string nom { get; set; }
    public string email { get; set; }
  }
}
