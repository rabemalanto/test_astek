using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace board.domain.Entities
{
  public class QuestionPageItem { 
    public Guid id { get; set; }
    public string subject { get; set; }
    public FormType SujetType { get; set; }
    [ForeignKey("QuestionPage")]
    public Guid idQuestionPage { get; set; }

    public string subSubject { get; set; }
    public virtual QuestionPage QuestionPage { get; set; }

  }

  public enum FormType
  {
    textBox,
    dropdownList,
    listOfRradio,
    checkbox
  }
}
