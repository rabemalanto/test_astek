using board.domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace board.domain.Concrete
{
  public class QuestionContext: DbContext
  {
    public QuestionContext(DbContextOptions<QuestionContext> opts): base(opts) { }

    public DbSet<Answer> answer { get; set; }
    public DbSet<Question> question { get; set; }
    public DbSet<QuestionPage> questionPage { get; set; }
    public DbSet<QuestionPageItem> questionPageItem { get; set; }
    public DbSet<User> user { get; set; }
  }
}
