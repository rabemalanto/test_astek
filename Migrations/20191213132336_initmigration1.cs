﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace board.Migrations
{
    public partial class initmigration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "question",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    titre = table.Column<string>(nullable: true),
                    idUserCreated = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_question", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "questionPage",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    titre = table.Column<string>(nullable: true),
                    rang = table.Column<int>(nullable: false),
                    idQuestion = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_questionPage", x => x.id);
                    table.ForeignKey(
                        name: "FK_questionPage_question_idQuestion",
                        column: x => x.idQuestion,
                        principalTable: "question",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "questionPageItem",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    SujetType = table.Column<int>(nullable: false),
                    idQuestionPage = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_questionPageItem", x => x.id);
                    table.ForeignKey(
                        name: "FK_questionPageItem_questionPage_idQuestionPage",
                        column: x => x.idQuestionPage,
                        principalTable: "questionPage",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "answer",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    idQuestionPageItem = table.Column<Guid>(nullable: false),
                    response = table.Column<string>(nullable: true),
                    idUser = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_answer", x => x.id);
                    table.ForeignKey(
                        name: "FK_answer_questionPageItem_idQuestionPageItem",
                        column: x => x.idQuestionPageItem,
                        principalTable: "questionPageItem",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_answer_idQuestionPageItem",
                table: "answer",
                column: "idQuestionPageItem");

            migrationBuilder.CreateIndex(
                name: "IX_questionPage_idQuestion",
                table: "questionPage",
                column: "idQuestion");

            migrationBuilder.CreateIndex(
                name: "IX_questionPageItem_idQuestionPage",
                table: "questionPageItem",
                column: "idQuestionPage");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "answer");

            migrationBuilder.DropTable(
                name: "questionPageItem");

            migrationBuilder.DropTable(
                name: "questionPage");

            migrationBuilder.DropTable(
                name: "question");
        }
    }
}
