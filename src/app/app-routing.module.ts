import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//login
import { AuthenticationGuard } from "./auth/authentication.guard";
import { AuthenticationComponent } from "./auth/authentication.component";
import { BoardResolve, QuestionnaireService } from './questionnaire/questionnaire.service';
import { QuestionPageComponent } from './questionnaire/board/question.page.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';

const routes: Routes = [
  { path: "login", component: AuthenticationComponent },
  {
    path: "questionnaires",
    canActivate: [AuthenticationGuard],
    component: QuestionnaireComponent,
    resolve: {
      scrumboard: QuestionnaireService
    }
  },
  {
    path: 'questionnaires/:id/:uri',
    canActivate: [AuthenticationGuard],
    component: QuestionPageComponent,
    resolve: {
      board: BoardResolve
    }
  },

  {
    path: "",
    redirectTo: "questionnaires"
    , pathMatch: "full"
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
