import { Injectable } from "@angular/core";
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthenticationService } from "./authentication.service";
@Injectable()
export class AuthenticationGuard {
  constructor(
    private authService: AuthenticationService, private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (this.authService.getToken()) {
      return true;
    } else {
      this.authService.callbackUrl = route.url.toString();
      this.router.navigateByUrl("/login");
      return false;
    }
  }
}
