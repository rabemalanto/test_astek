import { Injectable } from "@angular/core";
import { Repository } from "../models/repository";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
import "rxjs/add/observable/of";
import { of } from 'rxjs';
@Injectable()
export class AuthenticationService {
  constructor(private router: Router, private repo: Repository) { }
  authenticated: boolean = false;
  token: string;
  callbackUrl: string;
  login( loginForm ): Observable<boolean> {
    this.authenticated = false;
    return this.repo.login(loginForm.value.email, loginForm.value.password)
      .map(response => {
        if (response) {
          console.log('response.token', response);
          
          this.authenticated = true;
          loginForm.password = null;
          this.router.navigateByUrl("questionnaires");
        }
        return this.authenticated;
      })
      .catch(e => {
        this.authenticated = false;
        console.log("erreur", e);
        return of(false);
      });
  }
  getToken() {
    if (!this.token) this.token = localStorage.getItem('utoken');
    return this.token;
  }
  logout() {
    this.authenticated = false;
    this.repo.logout();
    this.router.navigateByUrl("/login");
  }
}
