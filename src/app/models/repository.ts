import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import "rxjs/add/operator/map";
import { Filter, Pagination } from "./configClasses.repository";
import { ErrorHandlerService, ValidationError } from "../errorHandler.service";
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import "rxjs/add/operator/catch";

const analyticUrl = "/analytics";

@Injectable()
export class Repository{
    private filter = new Filter();
    private pagination = new Pagination();

  constructor(private http: Http, private httpClient: HttpClient) { }
  private baseUrl = window.location.origin + '/api/';
  private token: string;
  //login
  login(name: string, password: string): Observable<any> {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(this.baseUrl + "/auth/login", { name: name, password: password }, { headers: headers }).map((response: Response) => {
      let token = response.json() && response.json().token;
      let user = response.json().user;
      console.log("response.json ", user);
      localStorage.setItem('utoken', token);
      localStorage.setItem('user', JSON.stringify(user));
      if (token) {
        return token;
      }
      else {
        return null;
      }
    });
  }

  
  logout(): void {
    
    localStorage.removeItem('utoken');
    localStorage.removeItem('user');
  }

  getUserLoggedIn() {
    if (this.token)
      return true;

    return false;
  }


}


