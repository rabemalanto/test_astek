import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { AuthenticationService } from "../auth/authentication.service";
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class QuestionnaireService implements Resolve<any>
{
    questionnaires: any[];
    routeParams: any;
    questionnaire: any;
    headers: HttpHeaders;
    onQuestionsChanged: BehaviorSubject<any>;
    onQuestionChanged: BehaviorSubject<any>;


    constructor(
      private _httpClient: HttpClient,
      private authService: AuthenticationService,
      private router: Router
    )
    {
        this.onQuestionsChanged = new BehaviorSubject([]);
      this.onQuestionChanged = new BehaviorSubject([]);
      this.headers = new HttpHeaders({ 'Authorization': "Bearer " + this.authService.getToken() });
    }

  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    if (err.status === 401 || err.status === 403) {
      localStorage.removeItem('utoken');
      localStorage.removeItem('user');
      this.router.navigateByUrl(`/login`);
      return of(err.message);
    }
    return throwError(err);
  }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {
            Promise.all([
                this.getQuestionnaires()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getQuestionnaires(): Promise<any>
    {
        return new Promise((resolve, reject) => {
          this._httpClient.get('api/questionnaires', { headers: this.headers }).pipe(catchError(x => this.handleAuthError(x)))
                .subscribe((response: any) => {
                  this.questionnaires = response;
                  this.onQuestionsChanged.next(this.questionnaires);
                  resolve(this.questionnaires);
            }, reject) ;
        });
    }



    deleteQuestionnaire(questionnaireId): Promise<any>
    {
        return new Promise((resolve, reject) => {
          this._httpClient.delete('api/questionnaires/' + questionnaireId, { headers: this.headers })
                  .subscribe((response: any) => {
                    this.questionnaires = response;
                    this.onQuestionsChanged.next(this.questionnaires);
                    resolve(this.questionnaires);
                  }, reject);
          });
    }

    getQuestionnaire(questionnaireId): Promise<any>
    {
        return new Promise((resolve, reject) => {
          this._httpClient.get('api/questionnaires/' + questionnaireId, { headers: this.headers })
                .subscribe((response: any) => {
                  this.questionnaire = response;
                  this.getQuestionPages(this.questionnaire.id);
                    this.onQuestionChanged.next(this.questionnaire);
                  resolve(this.questionnaire);
                }, reject);
        });
    }

  getQuestionPages(questionnaireId): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.get('api/questionPages/' + questionnaireId, { headers: this.headers })
        .subscribe((response: any) => {
          
          this.questionnaire.questionPages = response;
          this.onQuestionChanged.next(this.questionnaire);
          resolve(this.questionnaire);
        }, reject);
    });
  }
  getQuestionPageItems(idQuestionPage): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.get('api/QuestionPageItems/' + idQuestionPage, { headers: this.headers })
        .subscribe((response: any) => {
          const list = this.questionnaire.questionPages.find((_list) => {
            if (_list.id === idQuestionPage) {
              _list.QuestionPageItems = response;
            }
          });
          this.questionnaires = response;
          this.onQuestionsChanged.next(this.questionnaires);
          resolve(this.questionnaires);
        }, reject);
    });
  }
    addQuestionnaire(newQuestionnaire){
        var idMedia = 0;
        let p = new Promise((resolve, reject) => {
          this._httpClient.post('api/questionnaires/', newQuestionnaire, { headers: this.headers })
                .subscribe(response => {
                    idMedia = response['id'];
                    this.onQuestionChanged.next(this.questionnaire);
                    resolve(this.questionnaire);
                }, reject);
        });
        return idMedia;
    }

  addQuestion(newQuestion): Promise<any>
  {
        return new Promise((resolve, reject) => {
          this._httpClient.post('api/QuestionPageItems', newQuestion, { headers: this.headers })
                .subscribe(response => {
                    //this.board = response;
                    this.questionnaire.questionPages.map((page) => {
                      if (page.id === newQuestion.idQuestionPage )
                        {
                        if (page.QuestionPageItems == null) page.QuestionPageItems =[];
                        return page.QuestionPageItems.push(response);
                        }
                    });

                  console.log("new this.questionnaire", this.questionnaire);
                    this.onQuestionChanged.next(this.questionnaire);
                  resolve(this.questionnaire);
                }, reject);
        });
    }

    addQuestionPage(questionPage): Promise<any>
    {
      questionPage.idQuestion = this.questionnaire.id;
      return new Promise((resolve, reject) => {
        this._httpClient.post('api/QuestionPages', questionPage, { headers: this.headers })
          .subscribe(response => {
            questionPage.id = response['id'];
            if (this.questionnaire.questionPages)
              this.questionnaire.questionPages.push(questionPage);
            else {
              this.questionnaire.questionPages = [];
              this.questionnaire.questionPages.push(questionPage);
            }
            this.onQuestionChanged.next(this.questionnaire);
            resolve(this.questionnaire);
          }, reject);
      });

    }
  removeQuestionPageItem(QuestionPageItemId, QuestionPageId): Promise<any> {

    const item = this.questionnaire.pages.find((_list) => {
      return _list.id === QuestionPageId;
    });
    const questionPageItem = item.QuestionPageItems.find((qItem) => {
      return qItem.id === QuestionPageItemId
    });
    
    const index = item.QuestionPageItems.indexOf(questionPageItem);

    item.QuestionPageItems.splice(index, 1);
    this.questionnaire.pages.map((_list) => {
      if (_list.id === QuestionPageId) return item;
      return _list;
    });
    return this.updateQuestionnaire();
  }
  removeQuestionPage(id): Promise<any>
  {
    const result = new Promise((resolve, reject) => {
      this._httpClient.delete('api/QuestionPages/' + id, { headers: this.headers })
        .subscribe(response => {
          console.log("delete ", response);
        }, reject);
    });
    const list = this.questionnaire.questionPages.find((_list) => {
            return _list.id === id;
    });

    if (list.QuestionPageItems) {
      for (const QuestionPageItem of list.QuestionPageItems) {
        //this.removeQuestionPageItem(QuestionPageItem.id, QuestioinPage);
      }
    }
        const index = this.questionnaire.questionPages.indexOf(list);

    this.questionnaire.questionPages.splice(index, 1);

    return result;

    
    }

   /* removeQuestion(questionId, questionPage?): void
    {
       /* const card = this.questionnaires.questions.find((_card) => {
          return _card.id === questionId;
        });

      if (questionPage )
        {
            const list = this.board.lists.find((_list) => {
                return listId === _list.id;
            });
            list.idCards.splice(list.idCards.indexOf(cardId), 1);
        }

        this.board.cards.splice(this.board.cards.indexOf(card), 1);

        this.updateBoard();
        
    }*/

    updateQuestionnaire(): Promise<any>
    {
        return new Promise((resolve, reject) => {
          this._httpClient.put('api/questionnaires/' + this.questionnaire.id, this.questionnaire, { headers: this.headers })
            .subscribe(response => {

                    this.onQuestionChanged.next(this.questionnaire);
                    resolve(this.questionnaire);
                }, reject);
        });
  }

  updateQuestionPage(page): Promise<any> {
    console.log("page ", page);
    return new Promise((resolve, reject) => {

      this._httpClient.put('api/QuestionPages/' + page.id, page, { headers: this.headers })
        .subscribe(response => {

          this.onQuestionChanged.next(this.questionnaire);
          resolve(this.questionnaire);
        }, reject);
    });
  }

  createNewQuestionnaire(question): Promise<any>
    {
        return new Promise((resolve, reject) => {
          this._httpClient.post('api/questionnaires/', question, { headers: this.headers })
              .subscribe(response => {
                    question.id = response['id'];
                    question.titre = response['titre'];
                    resolve(question);
                }, reject);
        });
    }
}

@Injectable()
export class BoardResolve implements Resolve<any>
{

    constructor(
        private _questionnaireService: QuestionnaireService
    )
    {
    }

    resolve(route: ActivatedRouteSnapshot): Promise<any>
    {
      return this._questionnaireService.getQuestionnaire(route.paramMap.get('id'));
    }
}
