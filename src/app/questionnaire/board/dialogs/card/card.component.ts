import { Component, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms/src/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatMenuTrigger } from '@angular/material';
import { Subject } from 'rxjs';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FuseUtils } from '@fuse/utils';

import { QuestionnaireService } from 'app/questionnaire/questionnaire.service';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector     : 'scrumboard-board-card-dialog',
    templateUrl  : './card.component.html',
    styleUrls    : ['./card.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuestionPagesItemDialogComponent implements OnInit, OnDestroy
{
    questionPagesItem: any;
    questionnaire: any;
    questionPages: any;

    toggleInArray = FuseUtils.toggleInArray;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    @ViewChild('checklistMenuTrigger')
    checklistMenu: MatMenuTrigger;

    @ViewChild('newCheckListTitleField')
    newCheckListTitleField;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<ScrumboardCardDialogComponent>} matDialogRef
     * @param _data
     * @param {MatDialog} _matDialog
     * @param {ScrumboardService} _questionnaireService
     */
    constructor(
        public matDialogRef: MatDialogRef<QuestionPagesItemDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _matDialog: MatDialog,
        private _questionnaireService: QuestionnaireService
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
      this._questionnaireService.onQuestionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(questionnaire => {
                this.questionnaire = questionnaire;

              this.questionPagesItem = this._data.questionPagesItem;

                this.questionPages = this.questionnaire.pages.find((_list) => {
                    return this._data.pageId === _list.id;
                });
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Remove due date
     */
    removeDueDate(): void
    {
        this.questionPagesItem.due = '';
        this.updateQuestionPageItem();
    }

    /**
     * Toggle subscribe
     */
    toggleSubscribe(): void
    {
        this.questionPagesItem.subscribed = !this.questionPagesItem.subscribed;

        this.updateQuestionPageItem();
    }

    /**
     * Toggle cover image
     *
     * @param attachmentId
     */
    toggleCoverImage(attachmentId): void
    {
        if ( this.questionPagesItem.idAttachmentCover === attachmentId )
        {
            this.questionPagesItem.idAttachmentCover = '';
        }
        else
        {
            this.questionPagesItem.idAttachmentCover = attachmentId;
        }

        this.updateQuestionPageItem();
    }

    /**
     * Remove attachment
     *
     * @param attachment
     */
    removeAttachment(attachment): void
    {
        if ( attachment.id === this.questionPagesItem.idAttachmentCover )
        {
            this.questionPagesItem.idAttachmentCover = '';
        }

        this.questionPagesItem.attachments.splice(this.questionPagesItem.attachments.indexOf(attachment), 1);

        this.updateQuestionPageItem();
    }

    /**
     * Remove checklist
     *
     * @param checklist
     */
    removeChecklist(checklist): void
    {
        this.questionPagesItem.checklists.splice(this.questionPagesItem.checklists.indexOf(checklist), 1);

        this.updateQuestionPageItem();
    }

    /**
     * Update checked count
     *
     * @param list
     */
    updateCheckedCount(list): void
    {
        const checkItems = list.checkItems;
        let checkedItems = 0;
        let allCheckedItems = 0;
        let allCheckItems = 0;

        for ( const checkItem of checkItems )
        {
            if ( checkItem.checked )
            {
                checkedItems++;
            }
        }

        list.checkItemsChecked = checkedItems;

        for ( const item of this.questionPagesItem.checklists )
        {
            allCheckItems += item.checkItems.length;
            allCheckedItems += item.checkItemsChecked;
        }

        this.questionPagesItem.checkItems = allCheckItems;
        this.questionPagesItem.checkItemsChecked = allCheckedItems;

        this.updateQuestionPageItem();
    }

    /**
     * Remove checklist item
     *
     * @param checkItem
     * @param checklist
     */
    removeChecklistItem(checkItem, checklist): void
    {
        checklist.checkItems.splice(checklist.checkItems.indexOf(checkItem), 1);

        this.updateCheckedCount(checklist);

        this.updateQuestionPageItem();
    }

    /**
     * Add check item
     *
     * @param {NgForm} form
     * @param checkList
     */
    addCheckItem(form: NgForm, checkList): void
    {
        const checkItemVal = form.value.checkItem;

        if ( !checkItemVal || checkItemVal === '' )
        {
            return;
        }

        const newCheckItem = {
            'name'   : checkItemVal,
            'checked': false
        };

        checkList.checkItems.push(newCheckItem);

        this.updateCheckedCount(checkList);

        form.setValue({checkItem: ''});

        this.updateQuestionPageItem();
    }

    /**
     * Add checklist
     *
     * @param {NgForm} form
     */
    addChecklist(form: NgForm): void
    {
        this.questionPagesItem.checklists.push({
            id               : FuseUtils.generateGUID(),
            name             : form.value.checklistTitle,
            checkItemsChecked: 0,
            checkItems       : []
        });

        form.setValue({checklistTitle: ''});
        form.resetForm();
        this.checklistMenu.closeMenu();
        this.updateQuestionPageItem();
    }

    /**
     * On checklist menu open
     */
    onChecklistMenuOpen(): void
    {
        setTimeout(() => {
            this.newCheckListTitleField.nativeElement.focus();
        });
    }

    /**
     * Add new comment
     *
     * @param {NgForm} form
     */
    addNewComment(form: NgForm): void
    {
        const newCommentText = form.value.newComment;

        const newComment = {
            idMember: '36027j1930450d8bf7b10158',
            message : newCommentText,
            time    : 'now'
        };

        this.questionPagesItem.comments.unshift(newComment);

        form.setValue({newComment: ''});

        this.updateQuestionPageItem();
    }

    /**
     * Remove card
     */
    removeCard(): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete the card?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
              this.matDialogRef.close();
              this._questionnaireService.removeQuestionPageItem(this.questionPagesItem.id, this.questionPages.id);
            }
        });
    }

    /**
     * Update card
     */
    updateQuestionPageItem(): void
    {
      //this._questionnaireService.updateQuestion(this.questionPagesItem);
    }
}
