import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { QuestionnaireService } from 'app/questionnaire/questionnaire.service';
import { QuestionPage } from 'app/questionnaire/question.page.model';

@Component({
  selector: 'scrumboard-board',
    templateUrl  : './board.component.html',
    styleUrls    : ['./board.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class QuestionPageComponent implements OnInit, OnDestroy
{
    question: any;

    // Private
    private _unsubscribeAll: Subject<any>;
    

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _location: Location,
      private _questionnaireService: QuestionnaireService
    )
    {
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
           this._questionnaireService.onQuestionChanged
            .pipe(takeUntil(this._unsubscribeAll))
             .subscribe(questionnaire => {
               console.log("questionnaire ", questionnaire);
               this.question = questionnaire;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * On list add
     *
     * @param newListName
     */
    onListAdd(newListName): void
    {
        if ( newListName === '' )
        {
            return;
        }

      this._questionnaireService.addQuestionPage(new QuestionPage({titre: newListName}));
    }

    /**
     * On board name changed
     *
     * @param newName
     */
    onBoardNameChanged(newName): void
    {
        this._questionnaireService.updateQuestionnaire();
        this._location.go('/questionnaires/' + this.question.id + '/' + this.question.uri);
    }

    /**
     * On drop
     *
     * @param ev
     */
    onDrop(ev): void
    {
      this._questionnaireService.updateQuestionnaire();
    }
}
