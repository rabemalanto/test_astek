import { Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { QuestionPageItem } from 'app/questionnaire/question.page.item.model';
import { QuestionPage } from 'app/questionnaire/question.page.model';
@Component({
    selector     : 'scrumboard-board-add-card',
    templateUrl  : './add-card.component.html',
    styleUrls    : ['./add-card.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuestionnaireBoardAddCardComponent implements OnInit
{
    formActive: boolean;
    form: FormGroup;
    formType: string;
    public questionSubsubItem: string[];
    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = true;
    separatorKeysCodes: number[] = [ENTER, COMMA];
    questionPage: QuestionPage;

    @Output()
    QuestionItemAdded: EventEmitter<any>;

    @Input()
    QuestionPage;

    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
      private _formBuilder: FormBuilder,
    )
    {
        // Set the defaults
        this.formActive = false;
      this.QuestionItemAdded = new EventEmitter();
      this.formType = 'textBox';
      this.questionSubsubItem = [];
      
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Open the form
     */
    openForm(): void
    {
        this.form = this._formBuilder.group({
          name: '',
          typeQuestion: 'textBox'
        });
        this.formActive = true;
        //this.focusNameField();
    }

    closeForm(): void {
      this.formActive = false;
    }
    add(event: MatChipInputEvent): void {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        this.questionSubsubItem.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }
    }

  ngOnInit(): void {
    //console.log("this.QuestionPage ", this.QuestionPage);
    this.questionPage = this.QuestionPage;
  }

    remove(fruit): void {
      const index = this.questionSubsubItem.indexOf(fruit);

      if (index >= 0) {
        this.questionSubsubItem.splice(index, 1);
        // à supprimer les items ici
      }
    }

    /**
     * On form submit
     */
    onFormSubmit(): void
    {
        if ( this.form.valid )
        {
            //const cardName = this.form.getRawValue().name;
            
            this.formActive = false;
            let questionItem = new QuestionPageItem({
              subject: this.form.get('name').value,
              subSubject: JSON.stringify(this.questionSubsubItem),
              SujetType: this.formType,
              idQuestionPage: this.questionPage.id
            });

            this.QuestionItemAdded.next(questionItem);

        }


    }

  checkQuestionType(e: MatRadioChange): void {
    this.formType = e.value;
  }
}

