import { Component,EventEmitter, Input, Output,  ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector     : 'response-card',
    templateUrl  : './response.component.html',
    styleUrls    : ['./response.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ResponseComponent
{
    @Input()
    type: any;

    @Output()
    test: EventEmitter<any>;

    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _formBuilder: FormBuilder
    )
    {
        // Set the defaults
        this.test = new EventEmitter();
    }
}
