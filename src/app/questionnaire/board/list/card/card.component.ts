import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormType } from 'app/questionnaire/question.page.item.model';
import * as moment from 'moment';

@Component({
    selector     : 'question-page-item',
    templateUrl  : './card.component.html',
    styleUrls    : ['./card.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuestionnaireBoardCardComponent implements OnInit
{
    @Input()
    questionPagesItem;

    questionnaire: any;
  questionItem: any;
  formType: FormType;

    /**
     * Constructor
     *
     * @param {ActivatedRoute} _activatedRoute
     */
    constructor(
        private _activatedRoute: ActivatedRoute
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
      this.questionItem = this.questionPagesItem;
      this.questionItem.subSubject = JSON.parse(this.questionItem.subSubject);
      console.log("this.questionPagesItem ", this.questionPagesItem);
      this.questionnaire = this._activatedRoute.snapshot.data.questionnaire;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Is the card overdue?
     *
     * @param cardDate
     * @returns {boolean}
     */
    isOverdue(cardDate): boolean
    {
        return moment() > moment(new Date(cardDate));
    }
}
