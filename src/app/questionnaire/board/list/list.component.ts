import { Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

import { QuestionnaireService } from 'app/questionnaire/questionnaire.service';
import { QuestionPageItem } from 'app/questionnaire/question.page.item.model';
import { QuestionPagesItemDialogComponent } from 'app/questionnaire/board/dialogs/card/card.component';

@Component({
    selector     : 'scrumboard-board-list',
    templateUrl  : './list.component.html',
    styleUrls    : ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuestionnaireBoardListComponent implements OnInit, OnDestroy
{
    questionnaire: any;
    dialogRef: any;

    @Input()
    page;

    @ViewChild(FusePerfectScrollbarDirective)
    listScroll: FusePerfectScrollbarDirective;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ActivatedRoute} _activatedRoute
     * @param {ScrumboardService} _scrumboardService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _questionnaireService: QuestionnaireService,
        private _matDialog: MatDialog
    )
    {
        // Set the private defaults
      this._unsubscribeAll = new Subject();

      
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
      this._questionnaireService.getQuestionPageItems(this.page.id);
      this._questionnaireService.onQuestionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(questionnaire => {
                this.questionnaire = questionnaire;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * On list name changed
     *   
     * @param newListName
     */
    onListNameChanged(newListName): void
    {
      this.page.titre = newListName;
      this._questionnaireService.updateQuestionPage(this.page);
    }

    /**
     * On card added
     *
     * @param newQuestion
     */
    addQuestionItem(newQuestion): void
    {
        if ( newQuestion === '' )
        {
            return;
        }

      this._questionnaireService.addQuestion(newQuestion);

        setTimeout(() => {
            this.listScroll.scrollToBottom(0, 400);
        });
    }

    /**
     * Remove list
     *
     * @param listId
     */
    removeList(listId): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Êtes vous sure de vouloir supprimer cette page?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
              this._questionnaireService.removeQuestionPage(this.page.id);
            }
        });
    }

    /**
     * Open card dialog
     *
     * @param cardId
     */
    openCardDialog(questionPagesItem): void
    {
        this.dialogRef = this._matDialog.open(QuestionPagesItemDialogComponent, {
            panelClass: 'scrumboard-card-dialog',
            data      : {
              questionPagesItem: questionPagesItem,
                pageId: this.page.id
            }
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {

            });
    }

    /**
     * On drop
     *
     * @param ev
     */
    onDrop(ev): void
    {
      this._questionnaireService.updateQuestionnaire();
    }
}
