import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { QuestionnaireService } from 'app/questionnaire/questionnaire.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector     : 'scrumboard-board-settings',
    templateUrl  : './settings.component.html',
    styleUrls    : ['./settings.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class QuestionnaireBoardSettingsSidenavComponent implements OnInit, OnDestroy
{
    board: any;
    view: string;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    private _unsubscribeAll: Subject<any>;
    constructor(
        private _questionnaireService: QuestionnaireService,
        private _router: Router,
        private _matDialog: MatDialog
    )
    {
        this.view = 'main';
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this._questionnaireService.onQuestionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(board => {
                this.board = board;
            });
    }

    ngOnDestroy(): void
    {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    toggleCardCover(): void
    {
      this.board.cardCoverImages = !this.board.cardCoverImages;
      this._questionnaireService.updateQuestionnaire();
    }

    toggleSubscription(): void
    {
        this.board.subscribed = !this.board.subscribed;
      this._questionnaireService.updateQuestionnaire();
    }

  deleteQuestionnaire()
  {

    this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
      disableClose: false
    });

    this.confirmDialogRef.componentInstance.confirmMessage = 'Êtes vous sure de vouloir supprimer ce questionnaire?';

    this.confirmDialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._questionnaireService.deleteQuestionnaire(this.board.id);
        this._router.navigate(['questionnaires/']);
      }
    });
        
    }
}
