import { QuestionPage } from './question.page.model';

export class Question {
  titre: string;
  uri: string;
  id: string;
  QuestionPages: QuestionPage[];

  /**
   * Constructor
   *
   * @param board
   */
  constructor(questionnaire) {
    this.titre = questionnaire.titre || 'Nouveau Questionnaires';
    this.uri = questionnaire.uri || 'new-questionnaires';
    this.QuestionPages = [];
  }
}
