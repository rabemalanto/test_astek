import { QuestionPageItem } from './question.page.item.model';
export class QuestionPage {
  titre: string;
  id: string;
  QuestionPageItems: QuestionPageItem[];
  number: number;
  idQuestion: string;

  /**
   * Constructor
   *
   * @param board
   */
  constructor(board) {
    this.titre = board.titre || 'Nouvelle page';
    this.number = 0;
  }
}
