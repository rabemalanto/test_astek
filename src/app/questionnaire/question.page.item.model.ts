export class QuestionPageItem {
  id: string;
  subject: string;
  subSubject: string[];
  SujetType: FormType;
  idQuestionPage: string;

  /**
   * Constructor
   *
   * @param questionPageItem
   */
  constructor(questionPageItem) {
    this.subject = questionPageItem.subject || 'Nouveau question';
    this.SujetType = questionPageItem.SujetType || FormType.textBox;
    this.subSubject = JSON.parse(questionPageItem.subSubject[0]) || [];
    this.idQuestionPage = questionPageItem.idQuestionPage || "";
  }

 
}

export enum FormType {
  textBox,
  dropdownList,
  listOfRradio,
  checkbox
}
