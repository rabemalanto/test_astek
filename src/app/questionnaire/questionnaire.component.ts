import { Component, OnDestroy, OnInit, ViewEncapsulation  } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { QuestionnaireService } from 'app/questionnaire/questionnaire.service';
import { Question } from 'app/questionnaire/question.model';

@Component({
    selector     : 'scrumboard',
    templateUrl  : './questionnaire.component.html',
    styleUrls: ['./questionnaire.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class QuestionnaireComponent implements OnInit, OnDestroy
{
    Questions: any[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {Router} _router
     * @param {QuestionnaireService} _questionnaireService
     */
    constructor(
        private  _router: Router,
        private _questionnaireService: QuestionnaireService,

    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
            this._questionnaireService.onQuestionsChanged
            .pipe(takeUntil(this._unsubscribeAll))
              .subscribe(questionnaires => {
                this.Questions = questionnaires;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Nouveau questionnaire
     */
    newQuestionnaire(): void
    {
      const newQuestion = new Question({});
      
      this._questionnaireService.createNewQuestionnaire(newQuestion).then((response) => {
        console.log("response ", response);
        //console.log("url ", 'questionnaires/' + response.id + '/' + response.uri);
        this._router.navigate(['questionnaires/' + response.id + '/' + response.uri]);
       // this._router.navigateByUrl('questionnaires/' + newQuestion.id + '/' + newQuestion.uri);
        });
    }
}
