import { FuseUtils } from '@fuse/utils';

export class QuestionPages
{
    id: string;
    name: string;
    idDossier: string;

    /**
     * Constructor
     *
     * @param list
     */
    constructor(list)
    {
        //this.id = list.id || FuseUtils.generateGUID();
        this.name = list.name || '';
        this.idDossier = '';
    }
}
