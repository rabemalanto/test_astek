import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule,
    MatProgressBarModule, MatRippleModule, MatSidenavModule, MatToolbarModule, MatTooltipModule, MatRadioModule, MatSelectModule
} from '@angular/material';
import { NgxDnDModule } from '@swimlane/ngx-dnd';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseMaterialColorPickerModule } from '@fuse/components';

import { BoardResolve, QuestionnaireService } from 'app/questionnaire/questionnaire.service';
import { QuestionnaireComponent } from 'app/questionnaire/questionnaire.component';
import { QuestionPageComponent } from 'app/questionnaire/board/question.page.component';
import { QuestionnaireBoardListComponent } from 'app/questionnaire/board/list/list.component';
import { QuestionnaireBoardCardComponent } from 'app/questionnaire/board/list/card/card.component';
import { QuestionnaireBoardEditListNameComponent } from 'app/questionnaire/board/list/edit-list-name/edit-list-name.component';
import { QuestionnaireBoardAddCardComponent } from 'app/questionnaire/board/list/add-card/add-card.component';
import { QuestionnaireBoardAddListComponent } from 'app/questionnaire/board/add-list/add-list.component';
import { QuestionPagesItemDialogComponent } from 'app/questionnaire/board/dialogs/card/card.component';
import { QuestionnaireLabelSelectorComponent } from 'app/questionnaire/board/dialogs/card/label-selector/label-selector.component';
import { QuestionnaireEditBoardNameComponent } from 'app/questionnaire/board/edit-board-name/edit-board-name.component';
import { QuestionnaireBoardSettingsSidenavComponent } from 'app/questionnaire/board/sidenavs/settings/settings.component';
import { ResponseComponent } from 'app/questionnaire/board/list/add-card/response/response.component';
@NgModule({
  declarations: [
    
        QuestionnaireComponent,
        QuestionPageComponent,
        QuestionnaireBoardListComponent,
        QuestionnaireBoardCardComponent,
        QuestionnaireBoardEditListNameComponent,
        QuestionnaireBoardAddCardComponent,
        QuestionnaireBoardAddListComponent,
        QuestionPagesItemDialogComponent,
        QuestionnaireLabelSelectorComponent,
        QuestionnaireEditBoardNameComponent,
        QuestionnaireBoardSettingsSidenavComponent,
        ResponseComponent
    ],
    imports        : [
      RouterModule,
      MatIconModule,
      //NgxDnDModule
      MatRadioModule,
        MatButtonModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatProgressBarModule,
        MatRippleModule,
        MatSidenavModule,
      MatToolbarModule,
      MatSelectModule,
        MatTooltipModule,
        NgxDnDModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseMaterialColorPickerModule
        
    ],
    providers      : [
        QuestionnaireService,
        BoardResolve
    ],
  //exports: [ RouterModule ],
  entryComponents: [QuestionPagesItemDialogComponent]
})
export class QuestionnaireModule
{
}
