import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        
        id       : 'dashboards',
        title    : 'Tableau de bord',
        translate: 'NAV.DASHBOARDS',
        type     : 'item',
        icon     : 'assessment',
        url      : '/analytics'
    },
    {
        id   : 'kanban',
        icon:'ballot',
        title: 'Gestion des demandes',
        type : 'item',
        url  : '/boards'
    },
    {
        id: 'message',
        title: 'Messages',
        type: 'item',
        icon: 'mail',
        url: '/message'
    },
    {
        id: 'todo',
        icon: 'assignment_turned_in',
        title: 'A faire',
        type: 'item',
        url: '/todo'
    },
    {
        id: 'session_story',
        icon: 'album',
        title: 'Historique des sessions',
        type: 'item',
        url: '/story'
    },
    {
        id: 'manage_user',
        icon:'ballot',
        title: 'Gestion des clients',
        type: 'item',
        url: '/manage_user'
    },
];
