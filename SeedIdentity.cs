using board.domain.Concrete;
using board.domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace board
{
  public class SeedIdentity
  {
    
      private const string adminUser = "admin";
      private const string adminPassword = "$rovA1*";
      private const string adminRole = "Administrator";
      public static void SeedUsersAsync(QuestionContext context)
      {
        context.Database.Migrate();
        if (context.user.FirstOrDefault(x => x.email == "roockio@gmail.com") == null)
        {
          User user = new User
          {
            nom = adminUser,
            email = "roockio@gmail.com",
            pasword = adminPassword
          };

          context.user.Add(user);
          context.SaveChanges();
          
        }
      }

    }

    
  
}
