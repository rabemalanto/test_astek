using board.domain.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace board
{
  public class SeedData
  {
    public static void SeedDatabase(QuestionContext context)
    {
      context.Database.Migrate();
      
    }
  }
}
