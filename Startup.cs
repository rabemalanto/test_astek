using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using board.domain.Concrete;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;



namespace board
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession(options => {
              options.IdleTimeout = TimeSpan.FromMinutes(60);
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            //services.AddDbContext<IdentityContext>(options => options.UseSqlServer(Configuration["Data:sqlserver:ConnectionString"]));
            services.AddDbContext<QuestionContext>(options => options.UseLazyLoadingProxies(false).UseSqlServer(Configuration["Data:sqlserver:ConnectionString"]));
      //services.AddDefaultIdentity<IdentityUser>().AddRoles<IdentityRole>().AddEntityFrameworkStores<IdentityContext>();
      //traitement Json
      
            services.AddMvc().AddJsonOptions(opts => {
              opts.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
              opts.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
              //opts.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
              opts.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
      
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "wwwroot";
            });
            var SecretKey = Encoding.ASCII.GetBytes(Configuration["Secret"]);
      //Configure JWT Token Authentication - JRozario
            services.AddAuthentication(auth =>
            {
              auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
              auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(token =>
            {
              token.RequireHttpsMetadata = false;
              token.SaveToken = true;
              token.TokenValidationParameters = new TokenValidationParameters
              {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(SecretKey),
                ValidateIssuer = true,
                      //Usually this is your application base URL - JRozario
                      ValidIssuer = Configuration["Host"],
                ValidateAudience = true,
                      //Here we are creating and using JWT within the same application. In this case base URL is fine - JRozario
                      //If the JWT is created using a web service then this could be the consumer URL - JRozario
                      ValidAudience = Configuration["Host"],
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
              };
            });
      
    }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
                      ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
      QuestionContext boardContext = serviceProvider.GetService<QuestionContext>();
      SeedIdentity.SeedUsersAsync(boardContext);
      if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }
                else
                {
                    app.UseExceptionHandler("/Home/Error");
                    //app.UseHsts();
                }

               // app.UseHttpsRedirection();
                app.UseStaticFiles();
                app.UseCookiePolicy();

                
                app.UseSession();

                /*app.Use(async (context, next) =>
                {
                  var JWToken = context.Session.GetString("JWToken");
                  if (!string.IsNullOrEmpty(JWToken))
                  {
                    context.Request.Headers.Add("Authorization", "Bearer " + JWToken);
                  }
                  await next();
                });*/
                //Add JWToken Authentication service - JRozario
                app.UseAuthentication();
                
                app.UseMvc(
                  routes =>
                {
                  routes.MapRoute(
                      name: "default",
                      template: "{controller=Home}/{action=Index}/{id?}/{uri?}");
                  routes.MapSpaFallbackRoute("angular-fallback",
                  new { controller = "Home", action = "Index" });

                });
      
      /*app.UseSpa(spa =>
        {
            spa.Options.SourcePath = "./";

            if (env.IsDevelopment())
            {
                spa.UseAngularCliServer(npmScript: "build");
                spa.Options.StartupTimeout = TimeSpan.FromSeconds(200);
          }
        });*/

    }
    }
}
