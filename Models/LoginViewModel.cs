using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace board.Models
{
  public class LoginViewModel
  {
    public string Name { get; set; }
    public string Password { get; set; }
    public bool Remember { get; set; }
  }
}
